Status of SMB3 in Samba

Event Vault

Submission Type Presentation

Category Developer

Biography

Michael is one of the main developers of Samba (www.samba.org),
specializing on file serving and clustering. He is a
principal software engineer in Red Hat's Storage Server
team. He has previously presented at the Linux Plumbers
Conference and the LinuxCon Europe and is a regular
speaker at the sambaXP conference and the Storage
Developer Conference.

Abstract

Samba is the most important open source SMB file serving
software, and arguably, one of the most important SMB
implementations out there. While the Active Directory Server
features has attracted a lot of attention with the release of
Samba 4.0 in 2012, a lot is currently happening in Samba's file
server in the last couple of years.

One of the most active areas of development is version 3 of the
SMB protocol, introduced by Microsoft to move focus from pure
workstation workload to server workload. SMB3 adds an abundance
of new features to the protocol.  In particular, Microsoft
catches up with Samba/CTDB to offer all-active clustering.

After an overview of the state of Samba's file server, this talk
presents the progress of SMB3 support in Samba and in particular
explains the challenges faced. Special emphasis is put on the
state of Multi-Channel and RDMA support aka SMB Direct.

Audience

This talk is for anyone interested in NAS, file serving, Samba
and particularly SMB3 from the perspective of a user or
developer. Administrators, file system and file server
developers, engineers of products using Samba or considering
it...

Experience Level

Intermediate

Benefits to the Ecosystem

This presentation will help understand where Samba's file server
is today and what to expect from the next releases. It will also
help understand some internals of Samba. 
